# frozen_string_literal: true

module Gitlab
  module QA
    module Report
      class BaseTestResults
        include Enumerable

        attr_reader :path

        def initialize(path)
          @path = path
          @results = parse
          @testcases = process
        end

        def each(&block)
          testcases.each(&block)
        end

        def write
          raise NotImplementedError
        end

        private

        attr_reader :results, :testcases

        def parse
          raise NotImplementedError
        end

        def process
          raise NotImplementedError
        end
      end
    end
  end
end
