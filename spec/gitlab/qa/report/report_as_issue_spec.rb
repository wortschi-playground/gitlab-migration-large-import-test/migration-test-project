# frozen_string_literal: true

describe Gitlab::QA::Report::ReportAsIssue do
  let(:klass) { Class.new(described_class) }

  describe '#invoke!' do
    let(:project) { 'valid-project' }
    let(:test_file_full) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
    let(:test_file_partial) { 'browser_ui/stage/test_spec.rb' }

    it 'checks that a project was provided' do
      subject = klass.new(token: 'token', input_files: 'file')

      expect { subject.invoke! }
        .to output(%r{Please provide a valid project ID or path with the `-p/--project` option!}).to_stderr
        .and raise_error(SystemExit)
    end

    it 'checks that input files exist' do
      subject = klass.new(token: 'token', input_files: 'no-file', project: project)

      expect { subject.invoke! }
        .to output(/Please provide valid JUnit report files. No files were found matching `no-file`/).to_stderr
        .and raise_error(SystemExit)
    end

    context 'when validating user permissions' do
      subject { klass.new(token: 'token', input_files: 'file', project: project) }

      before do
        allow(subject).to receive(:assert_input_files!)
        allow(::Gitlab).to receive(:user).and_return(Struct.new(:id).new(0))
      end

      it 'checks that the user has at least Maintainer access to the project' do
        expect(::Gitlab).to receive(:team_member).with(project, 0).and_return(Struct.new(:access_level).new(10))

        expect { subject.invoke! }
          .to output("You must have at least Maintainer access to the project to use this feature.\n").to_stderr
          .and raise_error(SystemExit)
      end

      it 'checks that the user is a member of the project' do
        stub_const("Gitlab::Error::NotFound", RuntimeError)

        expect(::Gitlab).to receive(:team_member).with(project, 0).and_raise(Gitlab::Error::NotFound)

        expect { subject.invoke! }
          .to output("You must have at least Maintainer access to the project to use this feature.\n").to_stderr
          .and raise_error(SystemExit)
      end
    end
  end
end
